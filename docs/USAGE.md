# Usage

This framework uses `Promise`s, `Error`s and decorators to create/handle routes. The following should give you an idea on how to use this logic.

This framework is at it's best when used with a **fail-fast** strategy since it's build around the idea that an error can be used as a response; Can't find a user? Don't create a response object with information, but throw an `UserNotFoundError` and map that in the controller to an informative `ResponseError` for the client to see. (see 'Responding to a request' below) The framework will transform your custom `ResponseError` into a proper response. If you don't use `ResponseErrors` the framework will return a generic error (`500`) to the client without any information about what when wrong. That way you will never expose private information to the client on a failing request.

The idea is that in your controllers to preform all the actions (via decorators) to translate the user request into something a `service` can use. The response (including throw errors) will be converted by the controller into a response understate by the client.

## Table of Contents
1. [Creating a new route/controller](#creating-a-new-routecontroller)
2. [Validating if the request body contains all the right fields](#validating-if-the-request-body-contains-all-the-right-fields)
3. [Validate and cast request parameters](#validate-and-cast-request-parameters)
4. [Validating a header](#validating-a-header)
5. [Checking for a valid access-token and getting the id of the access-token user](#checking-for-a-valid-access-token-and-getting-the-id-of-the-access-token-user)
6. [Responding to a request](#responding-to-a-request)
7. [Defining a service](#defining-a-service)
8. [Defining a model](#defining-a-model)
9. [Adding a migration](#adding-a-migration)
10. [Using the Database class](#using-the-database-class)
   1. [Executing a query](#executing-a-query)
   2. [Using transactions](#using-transactions)
11. [Using the logger](#using-the-logger)

## Creating a new route/controller
To create a new route you need a controller to define that route in. In the folder `controllers` create a new file with the following content:
```TypeScript
// controllers/example.controller.ts
import { Get, Delete, Controller, RequestController, LontraRequest } from 'lontra';
import { Response } from 'express';
// ...

@Controller('/user') // This defines the beginning of the path for every route below
export class ExampleController extends RequestController {
  @Get('/me') // this route will be /user/me
  getMe(req: LontraRequest): Promise<User> { // Please note the usage of LontraRequest
    return new User('Gyzie');
  }

  @Post('/new') // this route will be /user/new
  getMe(req: LontraRequest<ICreateUserInterface>, res: Response): Promise<User> { // Please note the usage of LontraRequest<ICreateUserInterface>
    return new User(req.body); // Because of LontraRequest<ICreateUserInterface> the body will have the type ICreateUserInterface.
  }

  @Delete('/delete')
  // ...

  @Put('/put')
  // ...
}
```
You have now created a new route, but it will not be loaded until you define it in `index.ts`.
```TypeScript
// ...
logger.info('Creating routes...');
initController(app, new AuthController());
initController(app, new ExampleController()); // Add your controller
logger.info('Finished creating routes\n');
// ...
```

## Validating if the request body contains all the right fields
If you have a route that takes a request body and you wish to validate it's content to you can be sure you are passing the right object to the rest of the application use the `@AssertBody(ValidationSchema)` decorator. This decorator will check the incoming body and throw an `InvalidInputError` (and therefor ends the request) when the body does not match the required `schema`. Schemas are defined and validated via `Joi`. To validate a body you first need to create the schema. It's recommended that you define the schema aside the controller in the `contollers` folder.

```TypeScript
// contollers/auth.schemas.ts
import Joi from 'joi';

export interface ICreateUser {
  username: string;
  password: string;
  email?: string;
}

export const CreateUserSchema = Joi.object<ICreateUser>({
  username: Joi.string()
    .alphanum()
    .min(3)
    .max(64)
    .required(),
  password: Joi.string()
    .min(8)
    .max(255)
    .required(),
  email: Joi.string()
    .email()
    .optional(),
});
```
Once you have defined the schema you can use this schema on your routes:
```TypeScript
// controllers/auth.controller.ts
import { Get, Post, AssertBody, CreateUserSchema, LontraRequest } from 'lontra';
// ...

@Controller('/auth')
export class AuthController extends RequestController {
  @Post('/register')
  @AssertBody(CreateUserSchema) // Add the decorator with the schema as parameter.
  async createUser(req: LontraRequest<ICreateUser>): Promise<CustomResponse> { // Use LontraRequest<...> to define the type of the body.
    // You can now access the validated body via req.body (example: req.body.username)
    // ...
  }
```

## Validate and cast request parameters
You might have defined request parameters in express that you would like to cast and validate. Take for example the route `user/:id/:onlyName?`. This route takes an numric ID of the user and an optional boolean that makes the router only return the name of the user. If you use the `@AssertParameters` decorator you can validate and cast these parameters to a number and boolean before even reaching your function.

First create your schema to validate your params.
```TypeScript
// contollers/example.schemas.ts
import Joi from 'joi';

export interface IGetUserParams {
  id: number;
  onlyName?: boolean;
}

export const GetUserParamsSchema = Joi.object<IGetUserParams>({
  id: Joi.number().min(1).required(),
  onlyName: Joi.boolean(),
});
```
Once you have defined your schema can use this schema on your routes:
```TypeScript
// controllers/example.controller.ts
import { Get, Post, GetUserParamsSchema, GetUserSchema, IGetUserParams, LontraRequest } from 'lontra';
// ...

@Controller('/auth')
export class AuthController extends RequestController {
  @Post('/register')
  @AssertParameters(GetUserParamsSchema) // Add the decorator with the schema as parameter.
  async createUser(req: LontraRequest<any, IGetUserParams>): Promise<CustomResponse> { // Use LontraRequest<...> to define the type of the parameters.
    // You can now access the validated and casted parameters via req.parameters (example: req.parameters.id)
    // ...
  }
```

## Validating a header
Just like the request body you can validate a header to contain the right value. This is done via the `@AssertHeader` decorator. *Please remember that headers will always be strings and therefor can only be checked as a string.*

```TypeScript
@Post('/register')
@AssertHeader('example-header', Joi.string().min(5).required()) // Define the required header
// This request will now fail if it misses the header `example-header` and the length of the value is not at least 5
// Please note that if you use Joi.string().min(5) to define an optional header and the header is present but the value is '' the request will fail.
async createUser(req: LontraRequest<ICreateUser>): Promise<CustomResponse> {
  // ...
}
```

## Checking for a valid access-token and getting the id of the access-token user
If you have a route that is only accessible via a access-token you should use the `@AssertValidAccessToken()` decorator. This decorator will end the request with an error if the token is missing or invalid. If the token is valid it will provide the userId in the request.
```TypeScript
@Get('/me')
@AssertValidAccessToken()
getMe(req: TokenRequest): Promise<User> { // Use the TokenRequest type to get correct type hinting for getting req.userId
  // This request will only continue if the access-token is valid
  return UserService.getUser(req.userId, { includePrivateFields: true }); // get the userId via req.userId
}
```

## Responding to a request
With the info given above you can receive requests, but how do you respond to a request? Simply by returning the value you wish the user receives.

```TypeScript
@Get('/user')
async getMe(req: LontraRequest): Promise<User> { // Return an user object async
  return UserService.createUser('newUserName'); // returns a promise
}

@Get('/hey')
sup(): string { // Return a string
  return 'Olla';
}
```
In both cases the returned value will be send to the client as JSON with the response code `200`. If at some point an error is throw the application will return a generic error message with the status `500`. If you wish to provide the user with more informative errors you can map an error to another error type via the `@MapError([])` decorator. This decorator takes an array of types and mappers that will be used to map the error to a different error type.
```TypeScript
// types/response-error.type.ts
export class UsernameAlreadyExistsError extends RequestError {
  constructor() {
    super(409, 'UsernameAlreadyExistsError', 'An user with the given username already exists');
  }
}

// controller file
@Post('/user')
@MapError([
  { type: DuplicatedEntryError, mapper: err => new UsernameAlreadyExistsError() },
  { type: EmailAlreadyTakenError, mapper: ({ httpCode: 409, key: 'EmailAlreadyTakenResponseError', message: 'Cannot create account since email is already in use' }) },
])
async createUser(req: LontraRequest): Promise<User> { // Return an user object async
  return UserService.createUser('existingUserName'); // will throw the error DuplicatedEntryError
}
```
**Only errors of the type `RequestError` or `{ httpCode: number, key: string, message: string }` will be displayed to the client, all other error types will be shown as a generic error**

If you with to respond with a different status code than `200` you must return a `CustomResponse` object.
```TypeScript
@Get('/user')
async getMe(req: LontraRequest): Promise<CustomResponse> {
  const user = await UserService.createUser('newUserName');
  return new CustomResponse(user, 201); // user will be the response body and 201 the status code.
}

@Get('/hey')
sup(): string {
  return new CustomResponse('Olla', 203);
}
```

Sometimes you don't want to return JSON, but an image for example. If you wish to handel the response yourself you must return a `SelfHandledResponse` object. This object will cause the default response handling logic to be skipped. Therefor you must use the `res: Response` object to finish the request.
```TypeScript
@Get('/user')
getMe(req: LontraRequest, res: Response): SelfHandledResponse {
  res.write('I am writing my own response via the default Express functionality');
  res.end();
  return new SelfHandledResponse(); // Because of this the default response logic will not be called.
}
```

## Defining a service
Services are just static classes defined in the `services` folder. A service looks like:
```TypeScript
// services/auth.service.ts
export abstract class AuthService {
  private static readonly ACCESS_TOKEN_LIFETIME = '1h';
  private static readonly REFRESH_TOKEN_LIFETIME = '30 days';

  static async login({ username, password }: ILoginUser): Promise<TokenSet> {
  }
}
```
A service does not handle database request. This should be handled via repository.

## Defining a repository
A repository handles for example database requests. Define a repository in the `repositories` folder.
Repositories are just like services static classes, but they handle saving and retrieving data. A repository looks like:
```TypeScript
// repositories/user.repository.ts
export abstract class UserRepository {
  static async create({ username, password, email }: ICreateUser): Promise<User> {
    const passwordHash = await PasswordService.hash(password);
    await Database.query(`
      INSERT INTO Users (
        username, passwordHash, email
      ) VALUES (?, ?, ?);
    `, { values: [username, passwordHash, email] });
    return new User({ username, email });
  }
}
```

## Defining a model
Models should be defined in the `models` folder. To define a model implement the `IModel` interface.
```TypeScript
// models/user.repository.ts
// Define a septate interface for the model first
export interface IUser {
  username: string;
  email?: string;
}

// Create the User model/object by implementing your new interface.
export class User implements IUser, IModel<User> { // Implement the IModel<ModelClassType> interface
  readonly username: string;
  readonly email?: string;

  constructor({ username, email }: IUser) {
    this.username = username;
    if (email) this.email = email; // `if (email)` for when the db returns null
  }

  match(comparer: User): boolean { // Enforced by the IModel interface
    return this.username === comparer.username;
  }

  copy(): User { // Enforced by the IModel interface
    return new User(this);
  }
}
```

## Adding a migration
If you wish to change the database structure you must do this via a migration script. Migration scripts are just a list of objects with up and down commands. In these objects you define the steps that need to be taken to migrate the database. Make sure that every step only contains one SQL statement. For migrating you will use the same Database object that you use in your repositories.

If you wish to add a migration do it as follows:
1. Create a new migration file in the folder `migrations` like `migrations/YYYYMMDDHHmm-name.migration.ts` where `YYYYMMDDHHmm` should be the time of creation. For example `migrations/202009161730-example.migration.ts`.
1. In the new file export an `IMigrationSteps` object. This is an array of objects. The objects contain three keys:
`title`, `up` and `down`. `title` is used in the log for usability, `up` is a function that does the migration. `down` is a function that reverts the actions done by the `up` function. Make sure that every `up` and `down` only contain one SQL statement. If your migration needs multiple SQL statements add another object to the array.
1. Add your new migration script to the `index.ts` in the `migrations` folder. Provide your new migration with an unique `key` that is used for keeping track of executed migrations. It's recommended to use the filename of the migration as the `key`

`migrations/index.ts` looks as follows:
```TypeScript
import { IMigrationsCollection } from 'lontra';
import migration202009012005 from './202009012005-create_user.migration';
import migration202009021740 from './202009021740-create_refresh-token.migration'; // Import your script

const migrations: IMigrationsCollection = [
  { key: '202009012005-create_user', migration: migration202009012005 },
  { key: '202009021740-create_refresh-token', migration: migration202009021740 }, // Add your new migration
];

export default migrations; // Export the migrations
```

A migrations looks as follows:
```TypeScript
// migrations/202009021740-create_refresh-token.migration.ts
import { IDatabase, IMigrationSteps } from 'lontra';
const migration: IMigrationSteps = [
  { // Step 1
    title: 'Create refresh tokens table',
    up: (Database: IDatabase): Promise<void> => Database.query(`
      CREATE TABLE RefreshTokens
        (
          id    VARCHAR(255) NOT NULL,
          userId    INT(11) UNSIGNED NOT NULL,
          token     VARCHAR(255) NOT NULL UNIQUE,
          createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
          updatedAt TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (id),
          UNIQUE (token, userId)
        );
    `),
    down: (Database: IDatabase): Promise<void> => Database.query(`
      DROP TABLE RefreshTokens;
    `),
  },
  { // Step 2
    title: 'Add foreign key constraint for user to refresh token',
    up: (Database: IDatabase): Promise<void> => Database.query(`
      ALTER TABLE RefreshTokens
        ADD CONSTRAINT fkRefreshTokensUsers
        FOREIGN KEY (userId)
        REFERENCES Users(id) ON DELETE CASCADE ON UPDATE CASCADE;
    `),
    down: (Database: IDatabase): Promise<void> => Database.query(`
      ALTER TABLE RefreshTokens
        DROP INDEX fkRefreshTokensUsers;
    `),
  },
];

export default migration; // Export the migration
```

After you've added your migration to the `index.ts` in the `migrations` folder it will be executed at startup. Lontra known about these migrations since `migrations/index.ts` is added to the options of Lontra at startup
```TypeScript
// /index.ts
import migrations from './migrations';
// ...
const appOptions: ILontraOptions = {
  // ...
  migrations, // `migrations/index.ts` is provided to Lontra here
};
```

## Using the Database class
There is a `Database` object exposed by the `./index.ts` that should be used for communication with the database. It's a simple wrapper around the `MariaDB` driver.

### Executing a query
A simple query can be executed by calling the `query` function.
```TypeScript
const result = await Database.query<{ passwordHash: string }>(
      'SELECT passwordHash FROM Users WHERE username = ? LIMIT 1;',
      { values: [username] });
```

### Using transactions
If you wish to execute multiple statements in a transaction you must first create a transaction object and pass that object to all the `query` calls belonging to that transaction. Finally commit or cancel your transaction.
```TypeScript
const sql1 = '...';
const sql2 = '...';
let transaction: Transaction | undefined;
try {
  transaction = await Database.newTransaction();
  await Database.query(sql1, { transaction });
  await Database.query(sql2, { values: [1], transaction });
  await Database.commitTransaction(transaction);
} catch (err) {
  logger.error('Failed!', err);
  if (transaction) {
    await Database.rollbackTransaction(transaction);
  }
  throw err;
}
```

## Using the logger
It's not intended to use the default `console.log|error|warn` in this project. Instead use the logger class. The logger class is just a wrapper around `console.log|error|warn` witch add extra features.

At the top of you file define the logger:
```TypeScript
import { Logger } from 'lontra';
const logger = new Logger('database'); // provide the logger with a descriptive name
```
Use the logger in your code:
```TypeScript
logger.info('info message');
logger.error('error message', err);
logger.warn('warning message', 'something', { object: [] });
```
