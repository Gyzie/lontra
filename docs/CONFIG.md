# Config

Read about how to setup this framework

## Table of Contents
1. [Index.ts](#indexts)
2. [ILontraOptions](#ilontraoptions)

## Index.ts
To start add the following to your index.ts (or whatever you call it)
```TypeScript
import { IDatabaseConfig, MariaDbDatabase, Logger, ILontraOptions, Lontra } from 'lontra';
import { YouController } from './controllers/your.controller';
import migrations from './migrations';
const logger = new Logger('index');

// Add app settings
const appOptions: ILontraOptions = {
  appName: 'Lontra',
  port: 3000,
  pathToOpenApiJson: `${__dirname}/openapi.json`,
  migrations,
};

// Init your controllers
const appControllers = [
  new YouController(),
];

// Init and expose database if you need a database
const databaseOptions: IDatabaseConfig = {
  host: 'localhost',
  port: 3306,
  user: 'user',
  password: 'userpwd',
  database: 'appdb',
};
export const Database = new MariaDbDatabase(databaseOptions);

// Start the app
export const App = new Lontra(appOptions, appControllers, Database, (err) => {
  if (err) {
    logger.error('Failed to start app', err);
  } else {
    logger.info('App running');
  }
});
```

## ILontraOptions

### appName
Name to display in logs and Swagger

**Default:** 'Lontra'

### port
Listening port for requests

**Default:** 3000

### pathToOpenApiJson
File path of an OpenApi.json. If provided the app will display a SwaggerUI on `/`.

**Default:** undefined

### usePreMiddleware
Parameter to add third-party express middleware (array). See [Third-party middleware (Expressjs.com)](https://expressjs.com/en/guide/using-middleware.html#middleware.third-party).

**Default:** undefined

### corsOrigin
Set allowed cors origin.

**Default:** '*'

**Example:** ['https://yourapp.com', 'https://otherapp.com']

### trustProxy
If you are running behind a proxy and wish to get the client IP you must set the proxy to trust before the IP will be correctly resolved. See [Express behind proxies (Expressjs.com)](https://expressjs.com/en/guide/behind-proxies.html)

**Default:** undefined

**Example:** ['192.168.1.7', '86.21.87.234']
