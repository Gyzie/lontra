import express, { Application, RequestHandler } from 'express';
import * as http from 'http';
import cors from 'cors';
import swaggerUi from 'swagger-ui-express';
import { initController } from './server';
import { DatabaseMigrations, IMigrationsCollection } from './migrations';
import { Logger } from './logger';
import { IDatabase } from './database';
import { RequestController } from './types/request-controller.type';
import { IOpenApiInfo, OpenApi } from './openapi';
const logger = new Logger('lontra');

export interface ILontraOptions {
  appName?: string;
  port?: number;
  migrations?: IMigrationsCollection;
  usePreMiddleware?: RequestHandler[];
  corsOrigin?: string | string[];
  apiDocs?: IOpenApiInfo;
  /**
   * See https://expressjs.com/en/guide/behind-proxies.html
   */
  trustProxy?: string | (() => boolean) | boolean | number;
}

interface ILontraSettings extends ILontraOptions {
  appName: string;
  port: number;
}

export class Lontra {
  private readonly app: Application;
  private server?: http.Server;
  private readonly database?: IDatabase;
  private active = false;
  private onActiveCallbacks: (() => void)[] = [];
  private settings: ILontraSettings = {
    appName: 'Lontra',
    port: 3000,
  };

  constructor(options?: ILontraOptions, controllers?: (RequestController | [string, RequestHandler])[], database?: IDatabase, onStarted?: (err?: Error, app?: Application) => void) {
    this.settings = { ...this.settings, ...options };
    this.app = express();
    this.database = database;
    this.setup(controllers, database)
      .then(() => {
        if (onStarted) onStarted(undefined, this.app);
        for (const callback of this.onActiveCallbacks) {
          callback();
        }
        this.onActiveCallbacks = [];
      })
      .catch(err => {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        if (onStarted) onStarted(err);
      });
  }

  get express(): Application {
    return this.app;
  }

  private async setup(controllers?: (RequestController | [string, RequestHandler])[], database?: IDatabase): Promise<void> {
    const { app } = this;
    const { appName, port, migrations, usePreMiddleware, corsOrigin, apiDocs, trustProxy } = this.settings;

    logger.info(`Starting ${appName}... (${new Date().toString()})\n`);

    if (migrations && database) {
      logger.info('Running migrations...');
      await DatabaseMigrations.runMigrations(database, migrations);
      logger.info('Finished migrations\n');
    }

    if (trustProxy) {
      app.set('trust proxy', trustProxy);
      logger.info('Setup trusted proxy:', trustProxy);
    }

    app.use(express.json());

    app.use(cors({ origin: corsOrigin }));

    if (usePreMiddleware) {
      usePreMiddleware.forEach(middleware => app.use(middleware));
    }

    if (controllers) {
      logger.info('Creating routes...');
      controllers.forEach(controller => initController(app, controller));
      logger.info('Finished creating routes\n');
    }

    if (apiDocs) {
      logger.info('Setting up API docs...');
      const openApiSpec = OpenApi.buildDocument({ title: appName, ...apiDocs });
      app.get('/openapi.json', (_req, res) => {
        res.setHeader('Content-Type', 'application/json');
        res.send(openApiSpec);
      });
      logger.info('OpenAPI specs now available on: GET /openapi.json');
      app.use('/', swaggerUi.serve, swaggerUi.setup(openApiSpec));
      logger.info('API docs now running on: GET /');
      logger.info('Finished API docs setup\n');
    }

    return new Promise((resolve) => {
      this.server = app.listen(port, () => {
        logger.info(`${appName} up and listening on port ${port}\n`);
        this.active = true;
        resolve();
      });
    });
  }

  isActive(onActive?: () => void): boolean {
    if (this.active && onActive) onActive();
    if (!this.active && onActive) this.onActiveCallbacks.push(onActive);
    return this.active;
  }

  async stop(): Promise<void> {
    if (this.server) this.server.close();
    if (this.database) await this.database.endConnections();
  }
}
