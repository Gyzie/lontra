import chai from 'chai';
import chaiHttp from 'chai-http';
import chaiSpies from 'chai-spies';
import { before } from 'mocha';
import { App } from './test-server.spec';

export const request = (): ChaiHttp.Agent => chai.request(App.express);

chai.use(chaiHttp);
chai.use(chaiSpies);

before((done) => {
  App.isActive(() => done());
});

after(async() => {
  await App.stop();
});
