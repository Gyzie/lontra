import { Logger, ILontraOptions, Lontra, RequestController } from '.';
import { BodyDecoratorController } from './decorators/assert-body.decorator.spec';
import { HeaderDecoratorController } from './decorators/assert-header.decorator.spec';
import { AssertParametersDecoratorController } from './decorators/assert-parameters.decorator.spec';
import { ControllerDecoratorController } from './decorators/controller.decorator.spec';
import { MapErrorDecoratorController } from './decorators/map-error.decorator.spec';
import { ParamDecoratorController } from './decorators/param.decorator.spec';
import { RouteDecoratorController } from './decorators/route.decorator.spec';
const logger = new Logger('index');

// Add app settings
const appOptions: ILontraOptions = {
  appName: 'Lontra TST',
  port: 3000,
};

// Init your controllers
const appControllers: RequestController[] = [
  new RouteDecoratorController(),
  new ParamDecoratorController(),
  new MapErrorDecoratorController(),
  new ControllerDecoratorController(),
  new AssertParametersDecoratorController(),
  new HeaderDecoratorController(),
  new BodyDecoratorController(),
];

// Start the app
export const App = new Lontra(appOptions, appControllers, undefined, (err) => {
  if (err) {
    logger.error('Failed to start app', err);
  } else {
    logger.info('App running');
  }
});
