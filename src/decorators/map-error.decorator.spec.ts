import { expect } from 'chai';
import { request } from '../index.spec';
import { Controller, EntryNotFoundError, Get, RequestController, RequestError } from '..';
import { MapError } from '.';

class InheritError extends Error {
  constructor() {
    super('Inherit error');
  }
}

@Controller('/map-error')
export class MapErrorDecoratorController extends RequestController {
  @Get('/generic-object')
  @MapError([{ type: Error, mapper: { httpCode: 404, key: 'TestError', message: 'Test message' } }])
  genericObject(): string {
    throw new Error('test error');
  }

  @Get('/generic-object-multiple')
  @MapError([
    { type: Error, mapper: { httpCode: 404, key: 'TestError', message: 'Test message' } },
    { type: Error, mapper: { httpCode: 500, key: 'ShouldNotGetTo', message: 'Nothing' } },
  ])
  genericObjectMultiple(): string {
    throw new Error('test error');
  }

  @Get('/request-error')
  @MapError([{ type: EntryNotFoundError, mapper: () => new RequestError(400, 'TestRequestError', 'Test message') }])
  requestError(): string {
    throw new EntryNotFoundError();
  }

  @Get('/error-in-mapper')
  @MapError([
    { type: EntryNotFoundError, mapper: () => {
      throw new Error('Failing error-mapper');
    } }
  ])
  errorInMapper(): string {
    throw new EntryNotFoundError();
  }

  @Get('/no-match')
  @MapError([{ type: EntryNotFoundError, mapper: { httpCode: 403, key: 'ShouldNotGetTo', message: 'Nothing' } }])
  noMatch(): string {
    throw new Error();
  }

  @Get('/inherit-type')
  @MapError([{ type: Error, mapper: { httpCode: 403, key: 'ShouldResolveToThis', message: 'Something' } }])
  inheritType(): string {
    throw new InheritError();
  }

  @Get('/inherit-type-multiple')
  @MapError([
    { type: Error, mapper: { httpCode: 403, key: 'ShouldResolveToThis', message: 'Something' } },
    { type: InheritError, mapper: { httpCode: 404, key: 'ShouldNotGetTo', message: 'Noting' } },
  ])
  inheritTypeMultiple(): string {
    throw new InheritError();
  }
}

describe('Map Error decorator', () => {
  it('Should map generic error to provided error', async() => {
    const res = await request().get('/map-error/generic-object');
    expect(res).to.have.status(404);
    expect(res.body).to.deep.equal({ key: 'TestError', message: 'Test message' });
    expect(res.header).to.deep.include({ 'content-type': 'application/json', 'cache-control': 'no-store' });
  });

  it('Should resolve to first type match on multiple matches', async() => {
    const res = await request().get('/map-error/generic-object-multiple');
    expect(res).to.have.status(404);
    expect(res.body).to.deep.equal({ key: 'TestError', message: 'Test message' });
  });

  it('Should resolve to request error', async() => {
    const res = await request().get('/map-error/request-error');
    expect(res).to.have.status(400);
    expect(res.body).to.deep.equal({ key: 'TestRequestError', message: 'Test message' });
  });

  it('Should return generic error when mapping fails', async() => {
    const res = await request().get('/map-error/error-in-mapper');
    expect(res).to.have.status(500);
    expect(res.body).to.deep.equal({ key: 'RequestError', message: 'An unknown error occurred' });
  });

  it('Should return generic error on no match', async() => {
    const res = await request().get('/map-error/no-match');
    expect(res).to.have.status(500);
    expect(res.body).to.deep.equal({ key: 'RequestError', message: 'An unknown error occurred' });
  });

  it('Should match inherited types', async() => {
    const res = await request().get('/map-error/inherit-type');
    expect(res).to.have.status(403);
    expect(res.body).to.deep.equal({ key: 'ShouldResolveToThis', message: 'Something' });
  });

  it('Should match inherited types regarding of precision', async() => {
    const res = await request().get('/map-error/inherit-type-multiple');
    expect(res).to.have.status(403);
    expect(res.body).to.deep.equal({ key: 'ShouldResolveToThis', message: 'Something' });
  });
});
