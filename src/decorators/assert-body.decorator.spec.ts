import { expect } from 'chai';
import { request } from '../index.spec';
import { Controller, LontraRequest, Post, RequestController } from '..';
import Joi from 'joi';
import { AssertBody } from '.';

interface IBody {
  id: number;
  string: string;
  optional?: boolean;
}

const BodySchema = Joi.object<IBody>({
  id: Joi.number().min(1).required(),
  string: Joi.string().required(),
  optional: Joi.boolean(),
});

@Controller('/body-decorator')
export class BodyDecoratorController extends RequestController {
  @Post('/')
  @AssertBody(BodySchema)
  get(req: LontraRequest<IBody>): IBody {
    return req.body;
  }
}

describe('Assert body decorator', () => {
  it('Should allow correct body', async() => {
    const res = await request().post('/body-decorator')
      .send({
        id: 1,
        string: 'test',
        optional: true,
      });
    expect(res).to.have.status(201);
  });

  it('Should reject incorrect body', async() => {
    const res = await request().post('/body-decorator')
      .send({
        id: '1',
        string: 'test',
        optional: true,
      });
    expect(res).to.have.status(400);
    expect(res.body).to.deep.include({ key: 'InvalidInputError', message: 'Provided input was invalid' });
  });

  it('Should reject missing body', async() => {
    const res = await request().post('/body-decorator')
      .send();
    expect(res).to.have.status(400);
    expect(res.body).to.deep.include({ key: 'InvalidInputError', message: 'Provided input was invalid' });
  });

  it('Should reject empty body', async() => {
    const res = await request().post('/body-decorator')
      .send({});
    expect(res).to.have.status(400);
    expect(res.body).to.deep.include({ key: 'InvalidInputError', message: 'Provided input was invalid' });
  });

  it('Should not cast body', async() => {
    const res = await request().post('/body-decorator')
      .send({
        id: 1,
        string: 'test',
        optional: 'true',
      });
    expect(res).to.have.status(400);
    expect(res.body).to.deep.include({ key: 'InvalidInputError', message: 'Provided input was invalid' });
  });
});
