import { expect } from 'chai';
import { request } from '../index.spec';
import { Controller, EResponse, Get, RequestController } from '..';
import { AssertHeader } from '.';
import Joi from 'joi';

@Controller('/header-decorator')
export class HeaderDecoratorController extends RequestController {
  @Get('/')
  @AssertHeader('x-test', Joi.string().required())
  @AssertHeader('x-test-optional', Joi.string())
  get(): EResponse {
    return EResponse.OK;
  }
}

describe('Assert header decorator', () => {
  it('Should allow correct header', async() => {
    const res = await request().get('/header-decorator').set('x-test', 'something');
    expect(res).to.have.status(200);
  });

  it('Should reject incorrect header', async() => {
    const res = await request().get('/header-decorator').set('x-test', '');
    expect(res).to.have.status(400);
    expect(res.body).to.deep.include({ key: 'InvalidHeaderError', message: '"value" is not allowed to be empty' });
  });

  it('Should reject missing header', async() => {
    const res = await request().get('/header-decorator');
    expect(res).to.have.status(400);
    expect(res.body).to.deep.include({ key: 'InvalidHeaderError', message: '"value" is required' });
  });

  it('Should allow optional header', async() => {
    const res = await request().get('/header-decorator').set('x-test', 'required');
    expect(res).to.have.status(200);
  });
});
