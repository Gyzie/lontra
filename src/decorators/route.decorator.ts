import 'reflect-metadata';

export enum HttpVerb {
  DELETE = 'delete',
  GET = 'get',
  POST = 'post',
  PUT = 'put',
  PATCH = 'patch',
}

export interface IRouteMetadata {
  name: string;
  description?: string;
  path: string;
  httpVerb: HttpVerb;
}

export interface IRouteOptions {
  description?: string;
}

export const routeMetadataKey = Symbol('Route Metadata Key');

function addHttpVerbToMethodMetadata(target: object, propertyKey: string|symbol, httpVerb: HttpVerb, path: string, options: IRouteOptions): void {
  let metadata: IRouteMetadata = { name: propertyKey.toString(), path, httpVerb, ...options };
  const currentMetadata = Reflect.getOwnMetadata(routeMetadataKey, target, propertyKey) as IRouteMetadata|undefined ?? metadata;
  metadata = { ...currentMetadata, ...metadata };

  Reflect.defineMetadata(routeMetadataKey, metadata, target, propertyKey);
}

function helperForRoutes(httpVerb: HttpVerb, path: string, options: IRouteOptions): MethodDecorator {
  return (target: object, propertyKey: string|symbol): void => {
    const cleanPath = path.replace(/^\/+/, '');
    addHttpVerbToMethodMetadata(target, propertyKey, httpVerb, `/${cleanPath}`, options);
  };
}

export function Get(path = '', options: IRouteOptions = {}): MethodDecorator {
  return helperForRoutes(HttpVerb.GET, path, options);
}

export function Delete(path = '', options: IRouteOptions = {}): MethodDecorator {
  return helperForRoutes(HttpVerb.DELETE, path, options);
}

export function Post(path = '', options: IRouteOptions = {}): MethodDecorator {
  return helperForRoutes(HttpVerb.POST, path, options);
}

export function Put(path = '', options: IRouteOptions = {}): MethodDecorator {
  return helperForRoutes(HttpVerb.PUT, path, options);
}

export function Patch(path = '', options: IRouteOptions = {}): MethodDecorator {
  return helperForRoutes(HttpVerb.PATCH, path, options);
}
