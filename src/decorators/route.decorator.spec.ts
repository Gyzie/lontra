import { expect } from 'chai';
import { request } from '../index.spec';
import { Controller, Delete, EResponse, Get, LontraRequest, Patch, Post, Put, RequestController } from '..';

@Controller('/route')
export class RouteDecoratorController extends RequestController {
  @Get('/get')
  get(): string {
    return 'get';
  }

  @Post('/post')
  post(): string {
    return 'post';
  }

  @Put('/put')
  put(): string {
    return 'put';
  }

  @Patch('/patch')
  patch(): string {
    return 'patch';
  }

  @Delete('/delete')
  delete(): string {
    return 'delete';
  }

  @Get('/parameter/:id')
  parameter(req: LontraRequest): string {
    return req.params.id;
  }

  @Get('without-leading-slash')
  noLeadingSlash(): EResponse {
    return EResponse.OK;
  }

  @Get('////with-many-leading-slashes')
  manyLeadingSlashes(): EResponse {
    return EResponse.OK;
  }

  @Get('/regex/ab?cd')
  regex(): EResponse {
    return EResponse.OK;
  }

  @Get('/error')
  error(): EResponse {
    throw new Error('Test Error');
  }
}

describe('Route decorator', () => {
  it('Should accept get requests', async() => {
    const res = await request().get('/route/get');
    expect(res).to.have.status(200);
    expect(res.body).to.eq('get');
    expect(res.header).to.deep.include({ 'content-type': 'application/json' });
  });

  it('Should accept post requests', async() => {
    const res = await request().post('/route/post');
    expect(res).to.have.status(201);
    expect(res.body).to.eq('post');
    expect(res.header).to.deep.include({ 'content-type': 'application/json' });
  });

  it('Should accept put requests', async() => {
    const res = await request().put('/route/put');
    expect(res).to.have.status(200);
    expect(res.body).to.eq('put');
    expect(res.header).to.deep.include({ 'content-type': 'application/json' });
  });

  it('Should accept patch requests', async() => {
    const res = await request().patch('/route/patch');
    expect(res).to.have.status(200);
    expect(res.body).to.eq('patch');
    expect(res.header).to.deep.include({ 'content-type': 'application/json' });
  });

  it('Should accept delete requests', async() => {
    const res = await request().delete('/route/delete');
    expect(res).to.have.status(200);
    expect(res.body).to.eq('delete');
    expect(res.header).to.deep.include({ 'content-type': 'application/json' });
  });

  it('Should resolve requests /parameter/:id and return parameter', async() => {
    const res = await request().get('/route/parameter/test-parameter');
    expect(res).to.have.status(200);
    expect(res.body).to.eq('test-parameter');
  });

  it('Should resolve route definition without leading /', async() => {
    const res = await request().get('/route/without-leading-slash');
    expect(res).to.have.status(200);
    expect(res.body).to.eq(EResponse.OK);
  });

  it('Should resolve route definition with many leading /\'s', async() => {
    const res = await request().get('/route/with-many-leading-slashes');
    expect(res).to.have.status(200);
    expect(res.body).to.eq(EResponse.OK);
  });

  it('Should resolve regex in route', async() => {
    const res1 = await request().get('/route/regex/acd');
    expect(res1).to.have.status(200);
    expect(res1.body).to.eq(EResponse.OK);

    const res2 = await request().get('/route/regex/abcd');
    expect(res2).to.have.status(200);
    expect(res2.body).to.eq(EResponse.OK);

    const res3 = await request().get('/route/regex/ab?cd');
    expect(res3).to.have.status(404);
  });

  it('Should not display any error info besides default response on error', async() => {
    const res = await request().get('/route/error');
    expect(res).to.have.status(500);
    expect(res.body).to.deep.equal({ key: 'RequestError', message: 'An unknown error occurred' });
    expect(res.header).to.deep.include({ 'content-type': 'application/json', 'cache-control': 'no-store' });
  });
});
