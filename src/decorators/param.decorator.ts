import 'reflect-metadata';
import Joi, { BooleanSchema, NumberSchema, StringSchema } from 'joi';

type TParamSchema = BooleanSchema|NumberSchema|StringSchema;

const ALLOWED_PARAM_TYPES = ['string', 'boolean', 'number'];
type TValidPramType = 'string'|'boolean'|'number';

export interface IParamOptions {
  /**
   * A description that will be display in the OpenApi specs.
   */
  description?: string;
}

interface IParamMetadataEntry {
  key: string;
  validator: TParamSchema;
  description?: string;
}

export type TParamMetadata = IParamMetadataEntry[];

export const paramMetadataKey = Symbol('Parameter Metadata Key');

function getFunctionParameterNames(target: object, propertyKey: string | symbol): string[] {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const func = target[propertyKey] as Function|undefined; // eslint-disable-line
  if (!func) {
    throw new Error('Failed to get function for retrieving function parameters');
  }

  const functionString = func
    .toString()
    .replace(/((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg, ''); // clear comments
  const args = functionString
    .slice(functionString.indexOf('(') + 1, functionString.indexOf(')')) // Grab function list from string
    .match(/([^\s,]+)/g); // Grab parameters

  return args || [];
}

function getFunctionParameterTypes(target: object, propertyKey: string | symbol): string[] {
  const paramTypes = Reflect.getMetadata('design:paramtypes', target, propertyKey) as Function[];
  if (!paramTypes) {
    throw new Error('Cannot resolve type for Param decorator. Make sure you have emitDecoratorMetadata set to true in your tsconfig.json');
  }
  return paramTypes.map(paramType => paramType.name.toLowerCase());
}

function getFunctionParametersInfo(target: object, propertyKey: string | symbol): { name: string, type: string }[] {
  const names = getFunctionParameterNames(target, propertyKey);
  const types = getFunctionParameterTypes(target, propertyKey);
  return names.map((name, index) => ({ name, type: types[index] }));
}

function getDefaultValidatorForType(type: TValidPramType|string): TParamSchema {
  switch (type) {
    case 'string':
      return Joi.string().required();
    case 'boolean':
      return Joi.boolean().required();
    case 'number':
      return Joi.boolean().required();
    default:
      throw new Error(`Type '${type}' is not supported to get param validator for. Define a custom validator or change type.`);
  }
}

function isValidatorAndTypeCompatible(validator: Joi.Schema, type: TValidPramType|string) {
  return type === validator.type;
}

/**
 * Retrieve and validate parameter from the URL. When defining an URL you can add `:param` to it and use
 * this decorator to retrieve it's value. If you add a parameter to the URL and do not use this decorator
 * the parameter will be ignored. This means that it will also just show up as an undefined parameter in the
 * OpenApi specs.
 *
 * When defining the type of the parameter you are responsible for matching it with the type of the Joi schema.
 *
 * @example
 * import Joi from 'joi';
 * // ...
 *   \@Get('/:id', { description: 'Get otter by id' })
 *   getOtter(@Param(Joi.number(), { description: 'id of the otter' }) id: number): Promise<Otter> {
 * // ...
 *   \@Delete('/:id', { description: 'Delete otter by id' })
 *   deleteOtter(@Param('id', Joi.number(), { description: 'id of the otter' }) otterId: number): Promise<Otter> {
 * // ...
 *
 * @param key optional name of the parameter in the url without `:`. So for `:id` this value is `'id'`. If not set the name of the parameter will be used
 * @param validator optional Joi validation schema. The schema will always be considered required even if you set .optional(). If not set the default validation for given type will be applied (so for 'number' Joi.number())
 * @param options optional additional settings
 */
export function Param(options?: IParamOptions): ParameterDecorator;
export function Param(key: string, validator?: TParamSchema, options?: IParamOptions): ParameterDecorator;
export function Param(key: string, options?: IParamOptions): ParameterDecorator;
export function Param(validator: TParamSchema, options?: IParamOptions): ParameterDecorator;
export function Param(...args: (IParamOptions|TParamSchema|string|undefined)[]): ParameterDecorator {
  let key: string|undefined;
  let validator: TParamSchema|undefined;
  let options: IParamOptions = {};

  args.forEach((arg) => {
    if (typeof arg === 'undefined') {
      return;
    }
    if (typeof arg === 'string') {
      key = arg;
      return;
    }
    if (Joi.isSchema(arg)) {
      validator = arg;
      return;
    }
    options = arg;
  });

  return (target: object, propertyKey: string | symbol | undefined, parameterIndex: number): void => {
    if (!propertyKey) {
      throw new Error('Missing propertyKey for Param decorator');
    }

    const functionParameters = getFunctionParametersInfo(target, propertyKey);
    const functionParameter = functionParameters[parameterIndex];

    if (!ALLOWED_PARAM_TYPES.includes(functionParameter.type)) {
      throw new Error(`Param decorator for '${functionParameter.name}' cannot be of type '${functionParameter.type}'`);
    }

    const parameterKey = key || functionParameter.name;
    const paramValidator = validator || getDefaultValidatorForType(functionParameter.type);

    if (!isValidatorAndTypeCompatible(paramValidator, functionParameter.type)) {
      throw new Error(`Param decorator validator type and type are not compatible for '${functionParameter.name}'`);
    }

    const metadata: TParamMetadata = Reflect.getOwnMetadata(paramMetadataKey, target, propertyKey) as TParamMetadata|undefined ?? [];
    metadata[parameterIndex] = { key: parameterKey, validator: paramValidator.required(), ...options };

    Reflect.defineMetadata(paramMetadataKey, metadata, target, propertyKey);
  };
}
