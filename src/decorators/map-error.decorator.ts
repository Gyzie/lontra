import { Request, Response, NextFunction } from 'express';
import { RequestError } from '../types/request-error.type';

export type OriginalFunction = (req: Request, res: Response, next: NextFunction) => Promise<any>;
type OriginalTarget = any;
export type ErrorMapping = { type: new () => any, mapper: ((error: any) => RequestError)|{ httpCode: number, key: string, message: string }};

export function MapError(mappings: ErrorMapping[]) {
  return (_target: OriginalTarget, _propertyKey: string, descriptor: TypedPropertyDescriptor<any>): TypedPropertyDescriptor<any> => {
    const originalFunction = descriptor.value as OriginalFunction;
    descriptor.value = async function(req: Request, res: Response, next: NextFunction): Promise<any> {
      try {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-return
        return await originalFunction.apply(this, [req, res, next]);
      } catch (err) {
        for (const mapping of mappings) {
          if (err instanceof mapping.type) {
            throw typeof mapping.mapper === 'function' ? mapping.mapper(err) : new RequestError(mapping.mapper.httpCode, mapping.mapper.key, mapping.mapper.message);
          }
        }
        throw err;
      }
    };
    return descriptor;
  };
}
