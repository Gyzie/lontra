import { expect } from 'chai';
import { request } from '../index.spec';
import { Controller, Get, LontraRequest, RequestController } from '..';

import Joi from 'joi';
import { AssertParameters } from '.';

interface IParams {
  param1: number;
  param2: boolean;
  param3?: string;
}

const ParamsSchema = Joi.object<IParams>({
  param1: Joi.number().min(1).required(),
  param2: Joi.boolean().required(),
  param3: Joi.string(),
});

@Controller('/assert-parameters-decorator')
export class AssertParametersDecoratorController extends RequestController {
  @Get('/:param1/:param2/:param3?')
  @AssertParameters(ParamsSchema)
  get(req: LontraRequest<any, IParams>): IParams {
    return req.parameters;
  }
}

describe('Assert parameters decorator', () => {
  it('Should allow and cast correct parameters', async() => {
    const res = await request().get('/assert-parameters-decorator/5/true/3');
    expect(res).to.have.status(200);
    expect(res.body).to.deep.equal({ param1: 5, param2: true, param3: '3' });
  });

  it('Should not cast numeric boolean', async() => {
    const res = await request().get('/assert-parameters-decorator/5/1/3');
    expect(res).to.have.status(400);
    expect(res.body).to.deep.include({ key: 'InvalidRouteError', message: 'One or more parameters in the route are invalid' });
  });

  it('Should allow optional parameter', async() => {
    const res = await request().get('/assert-parameters-decorator/5/true');
    expect(res).to.have.status(200);
    expect(res.body).to.deep.equal({ param1: 5, param2: true });
  });

  it('Should not allow invalid input', async() => {
    const res = await request().get('/assert-parameters-decorator/false/true/test');
    expect(res).to.have.status(400);
    expect(res.body).to.deep.include({ key: 'InvalidRouteError', message: 'One or more parameters in the route are invalid' });
  });
});
