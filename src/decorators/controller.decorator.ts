import 'reflect-metadata';

export interface IControllerMetadata {
  name: string;
  description?: string;
  basePath: string;
}

export interface IControllerOptions {
  description?: string;
}

export const controllerMetadataKey = Symbol('Controller Metadata Key');

function setControllerMetadata(target: Function, basePath: string, options: IControllerOptions): void {
  const { name } = target;
  const prototype = target.prototype as object;

  let metadata: IControllerMetadata = { name, basePath, ...options };
  const currentMetadata = Reflect.getOwnMetadata(controllerMetadataKey, prototype) as IControllerMetadata | undefined ?? metadata;
  metadata = { ...currentMetadata, ...metadata };

  Reflect.defineMetadata(controllerMetadataKey, metadata, prototype);
}

export function Controller(path: string, options: IControllerOptions = {}): ClassDecorator {
  return <TFunction extends Function>(target: TFunction): void => {
    const cleanPath = path.replace(/^\/+/, '');
    setControllerMetadata(target, `/${cleanPath}`, options);
  };
}
