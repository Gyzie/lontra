import Joi from 'joi';
import { MissingBodyError } from '../types/request-error.type';
import { RequestController } from '../types/request-controller.type';
import { CreateMiddlewareReturn, createMiddlewareDecorator } from '.';

export function AssertBody(schema: Joi.ObjectSchema): CreateMiddlewareReturn {
  return createMiddlewareDecorator(req => {
    if (!req.body) {
      return Promise.reject(new MissingBodyError());
    }
    RequestController.assertValidity(schema.strict().required(), req.body); // Body schema must always be required if used in AssertBody
    return Promise.resolve();
  });
}
