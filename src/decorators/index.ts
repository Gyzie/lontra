import { Response, NextFunction } from 'express';
import { LontraRequest } from '..';

export type OriginalFunction = (req: LontraRequest, res: Response, next: NextFunction) => Promise<any>;
type OriginalTarget = any;
export type CreateMiddlewareReturn = (_target: any, _propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => TypedPropertyDescriptor<any>;

export function createMiddlewareDecorator(callback: OriginalFunction) {
  return (_target: OriginalTarget, _propertyKey: string, descriptor: TypedPropertyDescriptor<any>): TypedPropertyDescriptor<any> => {
    const originalFunction = descriptor.value as OriginalFunction;
    descriptor.value = async function(req: LontraRequest, res: Response, next: NextFunction): Promise<any> {
      await callback(req, res, next);
      if (originalFunction) {
        return originalFunction.apply(this, [req, res, next]);
      }
      return Promise.reject(new Error('Missing original function in decorator'));
    };
    return descriptor;
  };
}

export * from './assert-body.decorator';
export * from './assert-header.decorator';
export * from './assert-parameters.decorator';
export * from './controller.decorator';
export * from './map-error.decorator';
export * from './param.decorator';
export * from './route.decorator';
