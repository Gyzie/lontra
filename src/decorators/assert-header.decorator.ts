import Joi from 'joi';
import { InvalidHeaderError } from '../types/request-error.type';
import { createMiddlewareDecorator, CreateMiddlewareReturn } from '.';

export function AssertHeader(key: string, schema: Joi.StringSchema): CreateMiddlewareReturn {
  return createMiddlewareDecorator(req => {
    const header = req.header(key);
    const { error } = schema.validate(header);
    if (error) {
      return Promise.reject(new InvalidHeaderError(key, error.details[0].message));
    }
    return Promise.resolve();
  });
}
