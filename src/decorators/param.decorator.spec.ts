/* eslint-disable prefer-arrow-callback */
/* eslint-disable no-empty-function */
/* eslint-disable @typescript-eslint/no-empty-function */
import { expect } from 'chai';
import { request } from '../index.spec';
import { Controller, Get, RequestController } from '..';
import Joi from 'joi';
import { Param } from '.';
import { Request, Response } from 'express';

interface IParams {
  rootParam: string;
  param1: number;
  param2: boolean;
  param3?: string;
}

@Controller('/param-decorator/:rootParam')
export class ParamDecoratorController extends RequestController {
  @Get('ignore/:ignored')
  getIgnore(): object {
    return {};
  }

  @Get('/:param1/:param2/:param3?')
  get(
    @Param('rootParam') rootParam: string, // Test comment should not influence param resolvent )(),
    @Param(Joi.number().min(1)) param1: number, /* Test comment should not influence param resolvent ,)() */
    @Param(Joi.boolean()) param2: boolean,
      _req: Request, // Just some extra params to mess with parameter resolvent
      _res: Response, // Just some extra params to mess with parameter resolvent
    @Param('param3', Joi.string().optional()) param3?: string, // Optional should be ignored here
  ): IParams {
    return {
      rootParam,
      param1,
      param2,
      param3,
    };
  }
}

describe('Param decorator', () => {
  it('Should allow and cast correct parameters', async() => {
    const res = await request().get('/param-decorator/something/5/true/3');
    expect(res).to.have.status(200);
    expect(res.body).to.deep.equal({ rootParam: 'something', param1: 5, param2: true, param3: '3' });
  });

  it('Should not cast numeric boolean', async() => {
    const res = await request().get('/param-decorator/something/5/1/3');
    expect(res).to.have.status(400);
    expect(res.body).to.deep.include({ key: 'InvalidRouteError', message: 'One or more parameters in the route are invalid' });
  });

  it('Should treat optional parameters as required', async() => {
    const res = await request().get('/param-decorator/something/5/true');
    expect(res).to.have.status(400);
    expect(res.body).to.deep.include({ key: 'InvalidRouteError', message: 'One or more parameters in the route are invalid' });
  });

  it('Should not allow invalid input', async() => {
    const res = await request().get('/param-decorator/something/false/true/test');
    expect(res).to.have.status(400);
    expect(res.body).to.deep.include({ key: 'InvalidRouteError', message: 'One or more parameters in the route are invalid' });
  });

  it('Should allow ignored parameters', async() => {
    const res = await request().get('/param-decorator/something/ignore/123');
    expect(res).to.have.status(200);
  });

  function mockParamDecorator<T>(paramType: Function, decorator: ParameterDecorator) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const target = { func: (param: T) => undefined }; // Simulate class with method
    Reflect.defineMetadata('design:paramtypes', [paramType], target, 'func'); // Simulate TS adding metadata
    decorator(target, 'func', 0); // Simulate decoration of first parameter
  }

  it('Should throw on not allowed type', () => {
    expect(() => mockParamDecorator<Date>(function Object() {}, Param(Joi.string().isoDate()))).to.throw('Param decorator for \'param\' cannot be of type \'object\'', 'Date');
    expect(() => mockParamDecorator<undefined>(function Undefined() {}, Param(Joi.string()))).to.throw('Param decorator for \'param\' cannot be of type \'undefined\'', 'Undefined');
    expect(() => mockParamDecorator<null>(function Null() {}, Param(Joi.string()))).to.throw('Param decorator for \'param\' cannot be of type \'null\'', 'Null');
    expect(() => mockParamDecorator<bigint>(function BigInt() {}, Param(Joi.number()))).to.throw('Param decorator for \'param\' cannot be of type \'bigint\'', 'BigInt');
    expect(() => mockParamDecorator<symbol>(function Symbol() {}, Param(Joi.string()))).to.throw('Param decorator for \'param\' cannot be of type \'symbol\'', 'Symbol');
  });

  it('Should throw on not matching validator and type', () => {
    expect(() => mockParamDecorator<boolean>(function Boolean() {}, Param(Joi.string()))).to.throw('Param decorator validator type and type are not compatible for \'param\'', 'boolean > string');
    expect(() => mockParamDecorator<string>(function String() {}, Param(Joi.boolean()))).to.throw('Param decorator validator type and type are not compatible for \'param\'', 'string > boolean');
    expect(() => mockParamDecorator<number>(function Number() {}, Param(Joi.string()))).to.throw('Param decorator validator type and type are not compatible for \'param\'', 'number > string');
  });

  it('Should allow allowed types that match validator', () => {
    expect(() => mockParamDecorator<boolean>(function Boolean() {}, Param(Joi.boolean()))).not.to.throw();
    expect(() => mockParamDecorator<string>(function String() {}, Param(Joi.string()))).not.to.throw();
    expect(() => mockParamDecorator<number>(function Number() {}, Param(Joi.number()))).not.to.throw();
  });
});
