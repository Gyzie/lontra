import { expect } from 'chai';
import { request } from '../index.spec';
import { Controller, EResponse, Get, RequestController } from '..';

@Controller('////controller/:param/regex/ab?cd')
export class ControllerDecoratorController extends RequestController {
  @Get('/get')
  get(): EResponse {
    return EResponse.OK;
  }
}

describe('Controller decorator', () => {
  it('Should resolve multiple /\'s requests', async() => {
    const res = await request().get('/controller/4/regex/acd/get');
    expect(res).to.have.status(200);
    expect(res.body).to.eq(EResponse.OK);
  });

  it('Should resolve regex in controller', async() => {
    const res1 = await request().get('/controller/4/regex/acd/get');
    expect(res1).to.have.status(200);
    expect(res1.body).to.eq(EResponse.OK);

    const res2 = await request().get('/controller/4/regex/abcd/get');
    expect(res2).to.have.status(200);
    expect(res2.body).to.eq(EResponse.OK);

    const res3 = await request().get('/controller/4/regex/ab?cd/get');
    expect(res3).to.have.status(404);
  });
});
