import Joi from 'joi';
import { createMiddlewareDecorator, CreateMiddlewareReturn } from '.';
import { InvalidRouteError, LontraRequest } from '../types';

/**
 * @deprecated Use `@Param` parameter decorator to validate and retrieve parameters.
 */
export function AssertParameters<T = any>(parameterSchema: Joi.ObjectSchema): CreateMiddlewareReturn {
  return createMiddlewareDecorator((req: LontraRequest<any, T>) => {
    const { error, value } = parameterSchema.required().validate(req.params); // eslint-disable-line @typescript-eslint/no-unsafe-assignment
    if (error) {
      return Promise.reject(new InvalidRouteError(error));
    }
    req.parameters = value as T;
    return Promise.resolve();
  });
}
