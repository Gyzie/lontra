/*
  eslint-disable
  @typescript-eslint/no-unsafe-call,
  @typescript-eslint/no-unsafe-assignment
*/
import mariadb, { Pool, PoolConnection, PoolConfig } from 'mariadb';
import { IDatabase, IDatabaseConfig, Transaction, IQueryOptions, QueryValues, IQueryStats } from '.';
import { DuplicatedEntryError } from '../types/database-error.type';
import { Logger } from '../logger';
const logger = new Logger('mariadb');

export class MariaDbDatabase implements IDatabase {
  private readonly pool: Pool;

  constructor(config: IDatabaseConfig) {
    const poolConfig: PoolConfig = { ...config, timezone: 'Z', };
    this.pool = mariadb.createPool(poolConfig);
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  sanitizeValue(value: any): string {
    return this.pool.escape(value);
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  sanitizeId(value: string): string {
    return this.pool.escapeId(value);
  }

  async endConnections(): Promise<void> {
    await this.pool.end();
  }

  async newTransaction(): Promise<Transaction> {
    let conn: PoolConnection | undefined;
    try {
      conn = await this.pool.getConnection();
      await conn.beginTransaction();
      return conn;
    } catch (err) {
      if (conn) {
        await conn.end();
      }
      logger.error('Failed to get new transaction', err);
      throw err;
    }
  }

  async rollbackTransaction(transaction: Transaction): Promise<void> {
    await transaction.rollback();
    return transaction.end();
  }

  async commitTransaction(transaction: Transaction): Promise<void> {
    await transaction.commit();
    return transaction.end();
  }

  async query<T>(sql: string, { values, transaction }: IQueryOptions = {}): Promise<T> {
    let conn: PoolConnection | undefined = transaction;
    let result: T;
    const usesNamedPlaceholders = !Array.isArray(values);
    try {
      if (!conn) {
        conn = await this.pool.getConnection();
      }
      values = this.changeUndefinedIntoNull(values);
      const action = this.isBatchOperation(values) ? conn.batch : conn.query; // eslint-disable-line @typescript-eslint/unbound-method
      result = await action({ namedPlaceholders: usesNamedPlaceholders, sql }, values);
    } catch (err) {
      logger.error('Bad database request', err);
      throw this.resolveError(err);
    } finally {
      if (conn && !transaction) {
        await conn.end();
      }
    }
    return result;
  }

  batch(sql: string, options?: IQueryOptions): Promise<IQueryStats[]> {
    if (this.isBatchOperation(options?.values)) {
      return this.query<IQueryStats[]>(sql, options);
    }
    return Promise.reject(new Error('Values not specified as a batch operation'));
  }

  private isBatchOperation(values?: QueryValues): boolean {
    if (values && Array.isArray(values)) {
      for (const value of values) {
        if (Array.isArray(value) || (typeof value === 'object' && value !== null)) {
          return true;
        } else {
          return false;
        }
      }
    }
    return false;
  }

  private resolveError(error: any): any {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    const { code } = error;
    if (code) {
      switch (code) {
        case 'ER_DUP_ENTRY':
          return new DuplicatedEntryError();
      }
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return error;
  }

  /**
   * Use this function to convert JavaScripts `undefined` into `null`. Using
   * `undefined` as a value in a SQL statement will put `undefined` in the DB
   * instead of `null`. Use this function to resolve this issue.
   * @param values list of values for a query.
   */
  private changeUndefinedIntoNull(values?: QueryValues): QueryValues|undefined {
    if (values && Array.isArray(values)) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      return values.map(value => value ?? null);
    }
    return values;
  }
}

/* eslint-enable */
