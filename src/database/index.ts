import { PoolConnection } from 'mariadb';

export type Transaction = PoolConnection;

export type QueryValues = any[]|{ [key: string]: any };

export interface IQueryOptions {
  values?: QueryValues;
  transaction?: Transaction;
}

export interface IQueryStats {
  affectedRows: number;
  insertId: number;
  warningStatus: number;
}

export interface IDatabaseConfig {
  host: string;
  port: number;
  user?: string;
  password?: string;
  database?: string;
}

export interface IDatabase {
  endConnections(): Promise<void>;
  newTransaction(): Promise<Transaction>;
  rollbackTransaction(transaction: Transaction): Promise<void>;
  commitTransaction(transaction: Transaction): Promise<void>;
  query<T>(sql: string, options?: IQueryOptions): Promise<T>;
  sanitizeValue(value: any): string;
  sanitizeId(value: string): string;
}

export * from './maria-db.driver';
