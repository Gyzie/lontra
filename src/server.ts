import { Request, Response, Application, RequestHandler } from 'express';
import { InvalidRouteError, RequestError } from './types/request-error.type';
import { performance } from 'perf_hooks';
import { Logger } from './logger';
import { IControllerMetadata, controllerMetadataKey } from './decorators/controller.decorator';
import { HttpVerb, IRouteMetadata, routeMetadataKey } from './decorators/route.decorator';
import { CustomResponse, SelfHandledResponse, StatusResponse } from './types/custom-response.type';
import { LontraRequest, RequestController } from './types';
import { OpenApi } from './openapi';
import { TParamMetadata, paramMetadataKey } from './decorators';
const logger = new Logger('server');

export function getDefaultStatusCode(routeMetadata: IRouteMetadata): number {
  return routeMetadata.httpVerb === HttpVerb.POST ? 201 : 200;
}

function promisify(callback: Function, ...params: unknown[]): Promise<unknown> {
  try {
    const result = callback(...params) as unknown;
    if (result instanceof Promise) {
      return result;
    }
    return Promise.resolve(result);
  } catch (error) {
    return Promise.reject(error);
  }
}

function replyWithResult(res: Response, data: unknown, routeMetadata: IRouteMetadata): void {
  if (data instanceof SelfHandledResponse) {
    return;
  }

  let status = getDefaultStatusCode(routeMetadata);
  let body = data;
  if (data instanceof CustomResponse) {
    status = data.status; // eslint-disable-line prefer-destructuring
    body = data.body; // eslint-disable-line prefer-destructuring
  } else if (data instanceof StatusResponse) {
    status = data.status; // eslint-disable-line prefer-destructuring
    body = data.response; // eslint-disable-line prefer-destructuring
  }

  if (body) {
    res.setHeader('Content-Type', 'application/json');
    res.status(status).end(JSON.stringify(body));
  } else {
    res.setHeader('Content-Length', 0);
    res.sendStatus(status).end();
  }
}

function replyWithError(res: Response, error: unknown): void {
  let outputError: RequestError;
  if (error instanceof RequestError) {
    outputError = error;
    logger.requestError(error);
  } else {
    outputError = new RequestError();
    logger.error(error);
  }
  res.setHeader('Content-Type', 'application/json');
  res.setHeader('Cache-Control', 'no-store');
  res.status(outputError.httpCode).end(outputError.toJSON());
}

function getParameters(paramMetadata: TParamMetadata, req: Request, ...args: unknown[]): unknown[] {
  const params: unknown[] = [];
  const defaultParameters = [req, ...args]; // The default parameters (req, res) are added for backwards compatibility with Lontra v1.*.*

  for (let i = 0; i < paramMetadata.length; i++) {
    const param = paramMetadata[i];
    if (!param) {
      // Every parameter spot that did not use the @Param decorator will get filled by the default values or undefined if there are no defaults left
      params[i] = defaultParameters.shift();
      continue;
    }

    // Check and cast value
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    const { error, value } = param.validator.validate(req.params[param.key]);
    if (error) {
      throw new InvalidRouteError(error, param.key);
    }
    params[i] = value;
  }

  return [...params, ...defaultParameters];
}

function createAsyncRequestHandler(callBack: Function, routeMetadata: IRouteMetadata, paramMetadata: TParamMetadata): (req: Request, res: Response) => void {
  let start: number;
  return (req: Request, res: Response): void => {
    start = performance.now();
    /*
      Define the .parameters object so it will never be undefined. This object is used by
      the @AssertParameters decorator to set the parameters. And the LutraRequest object
      as this key defined as not-null therefor we must define it here.
    */
    (req as LontraRequest).parameters = {};

    let parameters: unknown[] = [];
    try {
      parameters = getParameters(paramMetadata, req, res);
    } catch (error) {
      replyWithError(res, error);
      return;
    }

    promisify(callBack, ...parameters)
      .then(data => replyWithResult(res, data, routeMetadata))
      .catch(error => replyWithError(res, error))
      .finally(() => {
        const end = performance.now();
        const requestDuration = Math.round((end - start) * 10) / 10;
        logger.info(`${req.method} ${req.url} [${res.statusMessage} ${res.statusCode}, ${requestDuration}ms]`);
      });
  };
}

export function initController(app: Application, path: string, requestHandler: RequestHandler): void
export function initController(app: Application, controller: RequestController): void
export function initController(app: Application, ...params: [RequestController]|[string, RequestHandler]): void {
  if (params.length === 2) {
    const [path, requestHandler] = params;
    app.use(path, requestHandler);
    logger.info(`[ROUTE]:\t[EXPRESS]\t${path}`);
    return;
  }
  const [controller] = params;

  const prototype = Object.getPrototypeOf(controller) as object;
  const controllerMetadata = Reflect.getOwnMetadata(controllerMetadataKey, prototype) as IControllerMetadata|undefined;
  if (!controllerMetadata) {
    return;
  }

  const { basePath } = controllerMetadata;
  OpenApi.registerController(controllerMetadata);

  let members: string[] = Object.getOwnPropertyNames(controller);
  members = members.concat(Object.getOwnPropertyNames(prototype));
  members.forEach(member => {
    const paramMetadata = Reflect.getOwnMetadata(paramMetadataKey, prototype, member) as TParamMetadata|undefined || [];
    const routeMetadata = Reflect.getOwnMetadata(routeMetadataKey, prototype, member) as IRouteMetadata|undefined;
    if (!routeMetadata) {
      return;
    }

    const { path, httpVerb }: IRouteMetadata = routeMetadata;
    const cleanPath = `${basePath}${path}`.replace(/\/+/g, '/');
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const requestHandler = controller[member].bind(controller) as Function; // eslint-disable-line
    app[httpVerb](cleanPath, createAsyncRequestHandler(requestHandler, routeMetadata, paramMetadata));
    logger.info(`[ROUTE]:\t${httpVerb.toUpperCase()}\t${cleanPath}`);
    OpenApi.registerPath(cleanPath, controllerMetadata, routeMetadata, paramMetadata);
  });
}
