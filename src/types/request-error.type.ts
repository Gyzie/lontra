import Joi from 'joi';

export class RequestError {
  httpCode: number;
  key: string;
  message: string;

  constructor(httpCode = 500, key = 'RequestError', message = 'An unknown error occurred') {
    this.httpCode = httpCode;
    this.key = key;
    this.message = message;
  }

  protected toResponse(): any {
    return {
      key: this.key,
      message: this.message,
    };
  }

  toJSON(): string {
    return JSON.stringify(this.toResponse());
  }
}

export class InvalidInputError extends RequestError {
  errors: { path: (string| number)[], message: string }[];

  constructor(error: Joi.ValidationError) {
    super(400, 'InvalidInputError', 'Provided input was invalid');
    this.errors = error.details.map(joiError => ({
      path: joiError.path,
      message: joiError.message,
    }));
  }

  toJSON(): string {
    return JSON.stringify({
      ...this.toResponse(),
      errors: this.errors,
    });
  }
}

export class InvalidRouteError extends RequestError {
  errors: { parameter: string|number, message: string }[];

  constructor(error: Joi.ValidationError, parameter?: string) {
    super(400, 'InvalidRouteError', 'One or more parameters in the route are invalid');
    this.errors = error.details.map(joiError => ({
      parameter: parameter || joiError.path[0],
      message: joiError.message,
    }));
  }

  toJSON(): string {
    return JSON.stringify({
      ...this.toResponse(),
      errors: this.errors,
    });
  }
}

export class MissingBodyError extends RequestError {
  constructor() {
    super(400, 'MissingBodyError', 'Request is missing body');
  }
}

export class InvalidHeaderError extends RequestError {
  header: string;

  constructor(header: string, message: string) {
    super(400, 'InvalidHeaderError', message);
    this.header = header;
  }

  toJSON(): string {
    return JSON.stringify({
      ...this.toResponse(),
      header: this.header,
    });
  }
}
