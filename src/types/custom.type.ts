import { Request as ExpressRequest } from 'express';
import { ParamsDictionary, Query } from 'express-serve-static-core';

export interface LontraRequest<T = any, J = any> extends ExpressRequest<ParamsDictionary, any, T, Query> {
  /**
   * @deprecated Use `@Param` parameter decorator to validate and retrieve parameters.
   */
  parameters: J;
}
