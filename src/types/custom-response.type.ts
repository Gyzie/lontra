export class CustomResponse {
  status: number;
  body: any;
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  constructor(body: any, status = 200) {
    this.status = status;
    // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
    this.body = body;
  }
}

export enum EResponse {
  OK = 'OK',
  DELETED = 'DELETED',
  UPDATED = 'UPDATED',
  CREATED = 'CREATED',
}

export class StatusResponse {
  status: number;
  response?: EResponse;

  constructor(status: number, response?: EResponse) {
    this.status = status;
    this.response = response;
  }
}

export class SelfHandledResponse {
}
