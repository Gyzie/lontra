export interface IModel<T> {
  match(comparer: T): boolean;
  copy(): T;
}
