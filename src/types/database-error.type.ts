export abstract class DatabaseError {
  message: string;
  constructor(message: string) {
    this.message = message;
  }
}

export class DuplicatedEntryError extends DatabaseError {
  constructor() {
    super('Entry with primary key already exists');
  }
}

export class EntryNotFoundError extends DatabaseError {
  constructor() {
    super('Entry not found');
  }
}
