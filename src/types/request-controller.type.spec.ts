import { expect } from 'chai';

import Joi from 'joi';
import { RequestController } from '.';

interface ISchema {
  param1: number;
  param2: boolean;
  param3?: string;
}

const schema = Joi.object<ISchema>({
  param1: Joi.number().min(1).required(),
  param2: Joi.boolean().required(),
  param3: Joi.string(),
});

describe('Request-controller class', () => {
  it('Should check for object validity: Invalid input', () => {
    expect(RequestController.assertValidity.bind(this, schema, {})).to.throw();
  });

  it('Should check for object validity: Valid input', () => {
    expect(RequestController.assertValidity(schema, { param1: 1, param2: true })).to.eq(true);
  });
});
