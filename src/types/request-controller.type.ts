import Joi from 'joi';
import { InvalidInputError } from '../types/request-error.type';

export abstract class RequestController {
  static assertValidity<T>(schema: Joi.ObjectSchema, value: T): boolean {
    const result = schema.validate(value, { abortEarly: false });
    if (result.error) {
      throw new InvalidInputError(result.error);
    }
    return true;
  }
}
