import swaggerUi from 'swagger-ui-express';
import { HttpVerb, IControllerMetadata, IRouteMetadata, TParamMetadata } from './decorators';
import { getDefaultStatusCode } from './server';

interface IPathParameter {
  key: string;
  description?: string;
  required: boolean;
  type?: string;
}

interface IPathResponse {
  code: number;
  description: string;
}

interface IPath {
  id: string;
  path: string;
  method: HttpVerb;
  controller: string;
  summary?: string;
  parameters: IPathParameter[];
  responses: IPathResponse[];
}

interface ITag {
  name: string;
  description?: string;
}

interface IOpenApiPathResponses {
  [key: string]: {
    description: string;
  }
}

interface IOpenApiPath {
  summary?: string;
  tags: string[];
  operationId: string;
  parameters: {
    name: string;
    in: 'path';
    description?: string;
    required: true;
    schema?: {
      type: string;
    };
  }[];
  responses: IOpenApiPathResponses;
}

interface IOpenApiPaths {
  [path: string]: {
    get?: IOpenApiPath;
    post?: IOpenApiPath;
    patch?: IOpenApiPath;
    put?: IOpenApiPath;
    update?: IOpenApiPath;
    delete?: IOpenApiPath;
  }
}

export interface IOpenApiInfo {
  /**
   * The title of the API. Defaults to appName
   */
  title?: string;
  /**
   * A description of the API. CommonMark syntax MAY be used for rich text representation.
   */
  description?: string;
  /**
   * A URL to the Terms of Service for the API. This MUST be in the form of a URL.
   */
  termsOfService?: string;
  /**
   * The contact information for the exposed API.
   */
  contact?: {
    /**
     * The identifying name of the contact person/organization.
     */
    name?: string;
    /**
     * The URL pointing to the contact information. This MUST be in the form of a URL.
     */
    url?: string;
    /**
     * The email address of the contact person/organization. This MUST be in the form of an email address.
     */
    email?: string;
  };
  /**
   * The license information for the exposed API.
   */
  license?: {
    /**
     * The license name used for the API.
     */
    name: string;
    /**
     * An SPDX license expression for the API. The identifier field is mutually exclusive of the url field.
     */
    identifier?: string;
    /**
     * A URL to the license used for the API. This MUST be in the form of a URL. The url field is mutually exclusive of the identifier field.
     */
    url?: string;
  };
}

export abstract class OpenApi {
  private static paths: IPath[] = [];
  private static tags: ITag[] = [];

  private static convertPath(path: string): string {
    return path.replaceAll(/:(\w+)\??/g, (_match: string, key: string) => `{${key}}`);
  }

  static registerController(controllerMetadata: IControllerMetadata): void {
    OpenApi.tags.push({
      name: controllerMetadata.name,
      description: controllerMetadata.description,
    });
  }

  static registerPath(path: string, controllerMetadata: IControllerMetadata, routeMetadata: IRouteMetadata, paramMetadata: TParamMetadata): void {
    const statusCode = getDefaultStatusCode(routeMetadata);

    OpenApi.paths.push({
      id: `${controllerMetadata.name}.${routeMetadata.name}`,
      path: OpenApi.convertPath(path),
      controller: controllerMetadata.name,
      method: routeMetadata.httpVerb,
      summary: routeMetadata.description,
      parameters: paramMetadata.map(param => {
        const type = param.validator.describe();
        return {
          key: param.key,
          description: param.description,
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore flags.presence is the actual key
          required: type.flags?.presence === 'required',
          type: type.type,
        };
      }),
      responses: [{ code: statusCode, description: 'unknown' }],
    });
  }

  private static buildResponses(responses: IPathResponse[]): IOpenApiPathResponses {
    const data: IOpenApiPathResponses = {};

    responses.forEach(response => {
      data[response.code] = {
        description: response.description,
      };
    });

    return data;
  }

  private static buildPaths(): IOpenApiPaths {
    const data: IOpenApiPaths = {};
    OpenApi.paths.forEach(path => {
      if (!data[path.path]) {
        data[path.path] = {};
      }

      data[path.path][path.method] = {
        operationId: path.id,
        tags: [path.controller],
        summary: path.summary,
        parameters: path.parameters.map(param => ({
          name: param.key,
          in: 'path',
          description: param.description,
          required: true, // Required when `in` = `path`
          schema: param.type ? { type: param.type } : undefined,
        })),
        responses: OpenApi.buildResponses(path.responses),
      };
    });
    return data;
  }

  private static buildTags(): ITag[] {
    return OpenApi.tags;
  }

  static buildDocument(info: IOpenApiInfo): swaggerUi.JsonObject {
    const version = process.env.npm_package_version || 'unknown';

    return {
      openapi: '3.0.0',
      info: {
        ...info,
        version,
      },
      tags: OpenApi.buildTags(),
      paths: OpenApi.buildPaths(),
    };
  }
}
