import { IDatabase } from './database';
import { Logger } from './logger';
const logger = new Logger('migrations');

export interface IMigrationStep {
  title: string;
  up: (Database: IDatabase) => Promise<void>;
  down: (Database: IDatabase) => Promise<void>;
}

export type IMigrationSteps = IMigrationStep[];

export type IMigrationsCollection = {
  key: string;
  migration: IMigrationSteps;
}[];

interface IMigration {
  key: string;
  runAt?: Date;
}

interface IMigrationRow {
  migration: string;
  runAt: string;
}

export class DatabaseMigrations {
  static async runMigrations(Database: IDatabase, collection: IMigrationsCollection): Promise<void> {
    const allMigrations = await DatabaseMigrations.getMigrationStatus(Database, collection);
    for (const migration of allMigrations) {
      if (migration.runAt) {
        logger.info(`[MIGRATION]: ${migration.key} SKIPPING\n\tWas run at ${migration.runAt.toString()}`);
      } else {
        logger.info(`[MIGRATION]: ${migration.key} EXECUTING...`);
        await DatabaseMigrations.executeMigration(migration, Database, collection);
        logger.info(`[MIGRATION]: ${migration.key} DONE`);
      }
    }
  }

  private static async executeMigration(migration: IMigration, Database: IDatabase, collection: IMigrationsCollection): Promise<void> {
    const executingMigration = collection.find(item => item.key === migration.key);
    if (!executingMigration) {
      throw new Error('Requested migration is missing from provided collection of migrations');
    }

    let successfully = true;
    const executedSteps: IMigrationStep[] = [];
    for (const step of executingMigration.migration) {
      try {
        await step.up(Database);
        executedSteps.push(step);
        logger.info(`\tStep '${step.title}' done`);
      } catch (err) {
        logger.error(`Migration failed at step '${step.title}'!`, err);
        successfully = false;
        break;
      }
    }

    if (successfully) {
      await Database.query(
        'INSERT INTO Migrations (migration) VALUES (?);',
        { values: [migration.key] }
      );
    } else {
      logger.error('Rolling back migration');
      await this.rollbackMigrationSteps(Database, executedSteps);
      throw new Error('Migration failed');
    }
  }

  private static async rollbackMigrationSteps(Database: IDatabase, steps: IMigrationStep[]): Promise<void> {
    for (const step of steps.reverse()) {
      await step.down(Database);
      logger.info(`\tStep '${step.title}' rolledback`);
    }
  }

  private static async getMigrationStatus(Database: IDatabase, collection: IMigrationsCollection): Promise<IMigration[]> {
    await DatabaseMigrations.initMigrationTable(Database);
    const runMigrations = await Database.query<IMigrationRow[]>('SELECT * FROM Migrations;');
    if (!Array.isArray(runMigrations)) {
      throw new Error('Expected runMigrations result to be an array, but it was not...');
    }

    const result: IMigration[] = [];
    for (const migration of collection) {
      const run = runMigrations.find(runMigration => runMigration.migration === migration.key);
      if (run) {
        result.push({ key: migration.key, runAt: new Date(run.runAt) });
      } else {
        result.push({ key: migration.key });
      }
    }
    return result;
  }

  private static async initMigrationTable(Database: IDatabase): Promise<void> {
    await Database.query(`
      CREATE TABLE IF NOT EXISTS Migrations(
        migration VARCHAR(255) NOT NULL ,
            runAt TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
            PRIMARY KEY (migration));
    `);
  }
}
