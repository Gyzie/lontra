/* eslint-disable no-console */

export enum ELogLevel {
  info = 'inf',
  waring = 'warn',
  error = 'err',
  requestError = 'rqr'
}

const suppressedLogLevels: { [level: string]: boolean } = {};

export const suppressLogLevels = (...levels: ELogLevel[]): void => {
  levels.forEach(level => (suppressedLogLevels[level] = true));
};

export const outputLogLevels = (...levels: ELogLevel[]): void => {
  levels.forEach(level => delete suppressedLogLevels[level]);
};

enum LogStyle {
  Reset = '\x1b[0m',
  Bright = '\x1b[1m',
  Dim = '\x1b[2m',
  Underscore = '\x1b[4m',
  Blink = '\x1b[5m',
  Reverse = '\x1b[7m',
  Hidden = '\x1b[8m',

  FgBlack = '\x1b[30m',
  FgRed = '\x1b[31m',
  FgGreen = '\x1b[32m',
  FgYellow = '\x1b[33m',
  FgBlue = '\x1b[34m',
  FgMagenta = '\x1b[35m',
  FgCyan = '\x1b[36m',
  FgWhite = '\x1b[37m',

  BgBlack = '\x1b[40m',
  BgRed = '\x1b[41m',
  BgGreen = '\x1b[42m',
  BgYellow = '\x1b[43m',
  BgBlue = '\x1b[44m',
  BgMagenta = '\x1b[45m',
  BgCyan = '\x1b[46m',
  BgWhite = '\x1b[47m',
}

export class Logger {
  private identifier: string;

  constructor(identifier: string) {
    this.identifier = identifier.substr(0, 8);
    while (this.identifier.length < 8) {
      this.identifier += '-';
    }
  }

  private getIdentifier(key: string, styles: LogStyle[]): string {
    const now = new Date();
    const hours = this.twoDigits(now.getHours());
    const minutes = this.twoDigits(now.getMinutes());
    const seconds = this.twoDigits(now.getSeconds());
    const time = `${hours}:${minutes}:${seconds}`;
    return `${styles.join('')}[${this.identifier}/${time}/${key.toUpperCase()}]${LogStyle.Reset}`;
  }

  private twoDigits(number: number): string {
    return `${number < 10 ? '0' : ''}${number}`;
  }

  info(...args: any[]): void {
    if (suppressedLogLevels[ELogLevel.info]) {
      return;
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    console.log(this.getIdentifier('inf', [LogStyle.Dim]), ...args);
  }

  error(...args: any[]): void {
    if (suppressedLogLevels[ELogLevel.error]) {
      return;
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    console.error(this.getIdentifier('err', [LogStyle.FgWhite, LogStyle.BgRed]), ...args);
  }

  requestError(...args: any[]): void {
    if (suppressedLogLevels[ELogLevel.requestError]) {
      return;
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    console.log(this.getIdentifier('rqr', [LogStyle.FgWhite, LogStyle.BgCyan]), ...args);
  }

  warn(...args: any[]): void {
    if (suppressedLogLevels[ELogLevel.waring]) {
      return;
    }
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    console.warn(this.getIdentifier('war', [LogStyle.FgYellow]), ...args);
  }
}

/* eslint-enable */
