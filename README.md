# Lontra

## About

Lontra is a little framework build around Express.js. It mainly adds decorators to help with reducing boilerplate code for creating and validating routs, but **you probably should not use it**. Lontra is a personal project that might be considered a simplified version of [NestJS](https://nestjs.com/). So you probably should use NestJS.

If you wish to try Lontra, check out:

1. [Config](https://gitlab.com/Gyzie/lontra/-/blob/develop/docs/CONFIG.md)
2. [Usage](https://gitlab.com/Gyzie/lontra/-/blob/develop/docs/USAGE.md)

## Install package
Install/update Lontra: `npm install --registry https://npm.gyzie.com lontra`

Or even better: Create `.npmrc` next to your `package.json` and add:
```
; Set a new registry for lontra package
registry=https://npm.gyzie.com
```
Now run `npm install lontra`.

## Contributing

### Publish package
Use the CLI to login into http://npm.gyzie.com (`npm login --registry=https://npm.gyzie.com --auth-type=legacy`) and run `npm publish --registry https://npm.gyzie.com`.
