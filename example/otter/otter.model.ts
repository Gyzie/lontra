import { IModel } from '../../src';

export interface IOtter {
  id: number;
  name: string;
}

export class Otter implements IOtter, IModel<Otter> {
  readonly id: number;
  readonly name: string;

  constructor({ id, name }: IOtter) {
    this.id = id;
    this.name = name;
  }

  match(comparer: Otter): boolean {
    return this.id === comparer.id;
  }

  copy(): Otter {
    return new Otter(this);
  }
}
