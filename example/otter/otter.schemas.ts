import Joi from 'joi';

export interface ICreateOtter {
  name: string;
}

export const CreateOtterSchema = Joi.object<ICreateOtter>({
  name: Joi.string()
    .min(1)
    .max(255)
    .required(),
});
