import { EntryNotFoundError } from '../../src';
import { IOtter, Otter } from './otter.model';

const otters: Otter[] = [
  new Otter({ id: 1, name: 'Piet' }),
  new Otter({ id: 2, name: 'Henk' }),
  new Otter({ id: 3, name: 'Anne' }),
];

export abstract class OtterRepository {
  static list(): Promise<Otter[]> {
    return Promise.resolve(otters.map(otter => otter.copy()));
  }

  static get(id: number): Promise<Otter> {
    const otter = otters.find(item => item.id === id);
    if (!otter) {
      return Promise.reject(new EntryNotFoundError());
    }
    return Promise.resolve(otter);
  }

  static create(otter: Omit<IOtter, 'id'>): Promise<Otter> {
    const newOtter = new Otter({ ...otter, id: otters.length });
    otters.push(newOtter);
    return Promise.resolve(newOtter);
  }
}
