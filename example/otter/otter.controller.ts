import Joi from 'joi';
import { Get, Controller, RequestController, AssertBody, LontraRequest, Post, Param } from '../../src';
import { Otter } from './otter.model';
import { CreateOtterSchema, ICreateOtter } from './otter.schemas';
import { OtterService } from './otter.service';

@Controller('/otter', { description: 'Manage otters' })
export class OtterController extends RequestController {
  @Get('/', { description: 'List all otters' })
  listOtters(): Promise<Otter[]> {
    return OtterService.list();
  }

  @Get('/:id', { description: 'Get otter by id' })
  getOtter(@Param(Joi.number().integer(), { description: 'id of the otter' }) id: number): Promise<Otter> {
    return OtterService.get(id);
  }

  @Post('/', { description: 'Create an otter' })
  @AssertBody(CreateOtterSchema)
  addOtter(req: LontraRequest<ICreateOtter>): Promise<Otter> {
    return OtterService.create(req.body);
  }
}
