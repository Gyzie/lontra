import { IOtter, Otter } from './otter.model';
import { OtterRepository } from './otter.repository';

export abstract class OtterService {
  static list(): Promise<Otter[]> {
    return OtterRepository.list();
  }

  static get(id: number): Promise<Otter> {
    return OtterRepository.get(id);
  }


  static create(otter: Omit<IOtter, 'id'>): Promise<Otter> {
    return OtterRepository.create(otter);
  }
}
