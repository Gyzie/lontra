import { Logger, ILontraOptions, Lontra, RequestController } from '../src';
import { CatController } from './cat/cat.controller';
import { OtterController } from './otter/otter.controller';
const logger = new Logger('index');

const appOptions: ILontraOptions = {
  appName: 'Lontra Example',
  port: 3000,
  apiDocs: {
    description: 'This is a sample server for a pet store.',
    termsOfService: 'https://example.com/terms/',
    contact: {
      name: 'API Support',
      url: 'https://www.example.com/support',
      email: 'support@example.com'
    },
    license: {
      name: 'Apache 2.0',
      url: 'https://www.apache.org/licenses/LICENSE-2.0.html'
    },
  },
};

const appControllers: RequestController[] = [
  new CatController(),
  new OtterController(),
];

export const App = new Lontra(appOptions, appControllers, undefined, (err) => {
  if (err) {
    logger.error('Failed to start app', err);
  } else {
    logger.info('App running');
  }
});
