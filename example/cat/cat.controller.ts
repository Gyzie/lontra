import { Get, Controller, RequestController, AssertBody, LontraRequest, Post } from '../../src';
import { Cat } from './cat.model';
import { CreateCatSchema, ICreateCat } from './cat.schemas';
import { CatService } from './cat.service';

@Controller('/cat', { description: 'Manage cats' })
export class CatController extends RequestController {
  @Get('/')
  listCats(): Promise<Cat[]> {
    return CatService.list();
  }

  @Post('/')
  @AssertBody(CreateCatSchema)
  addCat(req: LontraRequest<ICreateCat>): Promise<Cat> {
    return CatService.create(req.body);
  }
}
