import { IModel } from '../../src';

export interface ICat {
  name: string;
}

export class Cat implements ICat, IModel<Cat> {
  readonly name: string;

  constructor({ name }: ICat) {
    this.name = name;
  }

  match(comparer: Cat): boolean {
    return this.name === comparer.name;
  }

  copy(): Cat {
    return new Cat(this);
  }
}
