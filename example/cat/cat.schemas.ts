import Joi from 'joi';

export interface ICreateCat {
  name: string;
}

export const CreateCatSchema = Joi.object<ICreateCat>({
  name: Joi.string()
    .min(1)
    .max(255)
    .required(),
});
