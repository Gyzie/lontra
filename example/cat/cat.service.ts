import { ICat, Cat } from './cat.model';
import { CatRepository } from './cat.repository';

export abstract class CatService {
  static async list(): Promise<Cat[]> {
    return CatRepository.list();
  }

  static async create(cat: ICat): Promise<Cat> {
    return CatRepository.create(cat);
  }
}
