import { ICat, Cat } from './cat.model';

const cats: Cat[] = [
  new Cat({ name: 'Piet' }),
  new Cat({ name: 'Henk' }),
  new Cat({ name: 'Anne' }),
];

export abstract class CatRepository {
  static list(): Promise<Cat[]> {
    return Promise.resolve(cats.map(cat => cat.copy()));
  }

  static create(cat: ICat): Promise<Cat> {
    const newCat = new Cat(cat);
    cats.push(newCat);
    return Promise.resolve(newCat);
  }
}
