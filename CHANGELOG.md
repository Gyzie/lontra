2.0.0 (...)
* Add automatic api-docs generation
* Add @Param as replacement for @AssertParameter for grabbing and validating request parameters
* BREAKING Post endpoints will now return 201 by default instead of 200

1.4.1 (2023-02-19)
* Update readme with important information about what Lontra is

1.4.1 (2023-02-19)
* Update packages

1.4.0 (2022-01-23)
* Improve docs
* Add trust proxy parameter for servers behind a proxy
* Allow mapping errors to ResponseErrors via object
* Add requestError log option
* Fixed body being validated as casted

1.3.1 (2021-06-25)
* Add sanitize functions to database object

1.3.0 (2021-06-24)
* Remove template engine

1.2.4 (2021-06-24)
* Allow suppressing log

1.2.3 (2021-06-24)
* Expose express object via Lontra

1.2.2 (2021-04-11)
* Rename patch route decorator to Path for consistency

1.2.1 (2021-03-07)
* Resolve issue where the maria-db driver converts false to null.

1.2.0 (2021-02-22)
* Add Patch http request.
* Add support for adding express routers directly.

1.1.0 (2020-12-21)
* Add Handlebars as optional template engine.
* Allow setting CORS origin.

1.0.6 (2020-11-01)
* Allow objects with constructor params for map-error decorator.

1.0.5 (2020-11-01)
* Add setting for defining pre middleware.

1.0.4 (2020-11-01)
* Return express Application object in onStarted callback as second parameter.
